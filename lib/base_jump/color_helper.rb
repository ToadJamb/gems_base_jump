module BaseJump
  module ColorHelper
    extend self

    COLORS = [
      :cyan,
      :magenta,
      :blue,
    ]

    def colorize(*args)
      if Backpack.configuration.colorize? && args.count > 1
        ColorMeRad.colorize(args.shift.to_s, *args)
      else
        args.first
      end
    end

    def next_color
      COLORS[self.index += 1]
    end

    def colored?(text)
      ColorMeRad.colored? text
    end

    def index
      @index ||= -1
    end

    def index=(value)
      value = -1 unless COLORS[value + 1]
      @index = value
    end
  end
end
