module BaseJump
  module ShowStartupMessageCommand
    extend self

    def execute(delay = 5)
      show_unexpected if dangerous_branch_env?
      show_environment
      show_branch
      show_migrations if migrations_pending?
      pause(delay) if delay && delay > 0 && dangerous?
    end

    private

    def dangerous?
      dangerous_branch_env? || dangerous_migrations?
    end

    def pause(secs = nil)
      return if Backpack.app.irb?

      secs ||= 5

      message = "Pausing for #{secs} seconds in case you wish to cancel."
      puts colorize(message, :red)

      sleep secs

      puts colorize('continuing...', :green)

      sleep 2
    end

    def show_migrations
      color = dangerous_migrations? ? :red : :yellow
      puts colorize("There are pending migrations.", color)
    end

    def migrations_pending?
      return unless defined?(ActiveRecord)
      return @migrations_pending if defined?(@migrations_pending)

      @migrations_pending = false

      begin
        ActiveRecord::Migration.check_pending!
      rescue ActiveRecord::PendingMigrationError, ActiveRecord::NoDatabaseError
        @migrations_pending = true
      end

      @migrations_pending
    end

    def show_unexpected
      puts colorize('Unexpected environment/branch combination.', :light_red)
    end

    def dangerous_branch_env?
      Backpack.app.env.production? && branch != 'master'
    end

    def dangerous_migrations?
      Backpack.app.env.production? && migrations_pending?
    end

    def branch
      return @branch if defined?(@branch)

      branches = `git branch`
      branch = branches.match(/^\* ?(.*)/)[1]

      @branch = branch.strip
    end

    def show_branch
      color = branch == 'master' ? :yellow : :green
      color = :red if dangerous_branch_env?

      puts "Current branch is: #{colorize(branch, color)}"
    end

    def show_environment
      env   = Backpack.app.environment
      color = env_colors[env] || :red
      color = :red if dangerous_branch_env?

      puts "Loaded #{colorize env, color} environment."
    end

    def env_colors
      @env_colors ||= {
        :test        => :green,
        :development => :green,
        :production  => :yellow,
      }
    end

    def colorize(text, color)
      Backpack.app.colorize text, color
    end
  end
end
